/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import root.dto.Palabra;
import root.model.dao.PalabrasJpaController;

import root.model.dao.UsuarioDAO;
import root.model.entities.Palabras;
import root.model.entities.Usuario;



/**
 *
 * @author javi3
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            root.dto.Palabra pal = new  root.dto.Palabra();
             List<Palabra> significados; 
            System.out.print("doPost:" );
            String palabra1 = request.getParameter("palabra");
            //recuperar la palabra
            //servicio rest dicccionar local
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://localhost:8080/MantUsuarioResp-1.0-SNAPSHOT/api/diccionario/" + palabra1);
            
            significados = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "KEYSEC0001918").header("api-id", "111111").get( new GenericType<List< root.dto.Palabra>>(){});
            
            
           /* 
             List<Personas> personas = myResource.request(MediaType.APPLICATION_JSON)
        .header("api-key", "88888")
        .get(new GenericType<List<Personas>>(){});*/
 
           if (significados!= null){
            
               pal=significados.get(0);
            Palabras palabraBD=new Palabras();
            palabraBD.setPalabra(pal.getPalabra());
            palabraBD.setSignificado(pal.getDescripcion());
            
            PalabrasJpaController dao=new PalabrasJpaController();
            dao.create(palabraBD);
           }
            //grabar en nuestra base de datos
            
            /*  UsuarioDAO dao=new UsuarioDAO();
            List<Usuario> usuarios=dao.findUsuarioEntities();
            System.out.println("Cantidad clientes en Base de datos  "+usuarios.size())  ;*/
            
            //request.setAttribute("palabras", palabraBD);
            request.getRequestDispatcher("index.jsp").forward(request,response);
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}