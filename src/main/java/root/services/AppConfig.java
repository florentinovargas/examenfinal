/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import root.services.oxford.model.api.OxfordDef;

@ApplicationPath("api")
public class AppConfig extends Application{
    private final Logger log = Logger.getLogger(ApiOxford.class.getName());

    private final String prefixUrl = "https://od-api.oxforddictionaries.com/api/v2/entries/es/";
    private final String suffixUrl = "?fields=definitions&strictMatch=false";
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = new Gson();
    
    public OxfordDef getDefinitions(String word) throws IOException {

        //log.info("Environment::" + gson.toJson(System.getProperties()));
        String app_id = System.getProperty("OXFORD_APP_ID");
        String app_key = System.getProperty("OXFORD_APP_KEY");
      
        log.info("Request::app_id::" + app_id);
        log.info("Request::app_key::" + app_key);
        log.info("Request::Url" + prefixUrl + word + suffixUrl);

        Request request = new Request.Builder()
                                        .url(prefixUrl + word + suffixUrl)
                                        .get()
                                        .addHeader("accept", "application/json")
                                        .addHeader("app_id", "49b0e4aa")
                                        .addHeader("app_key", "b9ef15bac5378475715f6043bb27af54")
                                        //.addHeader("app_if", app_id)
                                        //.addHeader("app_key", app_key)
                                        .build();

        Response response = client.newCall(request).execute();

        OxfordDef body = gson.fromJson(response.body().string(), OxfordDef.class);

        log.info("Response::" + body.toString());

        return body;
    }

    
}
