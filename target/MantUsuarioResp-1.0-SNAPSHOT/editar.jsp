<%-- 
    Document   : index
    Created on : 09-may-2020, 20:04:51
    Author     : junior
--%>

<%@page import="root.model.entities.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  Usuario usuario=(Usuario)request.getAttribute("usuario");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Test Lista</title>
    </head>
    <body class="text-center" >
    <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <header class="masthead mb-auto">
        <div class="inner">
    
        </div>
      </header>
    </div>
      <main role="main" class="inner cover">
        <h1 class="cover-heading">Crear</h1>
        <p class="">
            
             <form  name="form" action="controllerUsuario" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input  name="username" value="<%= usuario.getUsername() %>" class="form-control" required id="username" aria-describedby="usernameHelp">
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  step="any" name="nombre" value="<%= usuario.getNombre()%>"  class="form-control" required id="nombre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                         <input  name="apellido" class="form-control"  value="<%= usuario.getApellido()%>"  required id="apellido" aria-describedby="apellidoHelp">
                 
                      </div>        
                    <br>
                    <button type="submit" name="accion" value="grabarEditar" class="btn btn-success">Grabar</button>
                     <button type="submit" name="accion" value="salirEditar"  class="btn btn-success">Salir</button>
                </form>
  
      </main>

  
      
       
    </body>
</html>